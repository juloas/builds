package fr.Vytalic.dofuxia.core.items;

import net.minecraft.item.Item;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class DJItems {

	public static void init() {
		
		// Item Initialization
	
	}

	@SuppressWarnings("unused")
	private static <T extends Item> Item register(T item) {
		
		GameRegistry.register(item);
		if ((item instanceof ItemModelProvider)) {
			
			((ItemModelProvider)item).registerItemModel(item);
			
		}
		
		return item;
		
	}

}
