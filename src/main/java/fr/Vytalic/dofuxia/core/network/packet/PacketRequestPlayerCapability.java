package fr.Vytalic.dofuxia.core.network.packet;

import fr.Vytalic.dofuxia.core.DofuxiaCore;
import fr.Vytalic.dofuxia.core.capabilities.IDofusCapabilities;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.server.FMLServerHandler;

public class PacketRequestPlayerCapability
implements IMessage {
    private String playerName;

    public PacketRequestPlayerCapability() {
    }

    public PacketRequestPlayerCapability(String player) {
        this.playerName = player;
    }

    public void fromBytes(ByteBuf buf) {
        this.playerName = ByteBufUtils.readUTF8String((ByteBuf)buf);
    }

    public void toBytes(ByteBuf buf) {
        ByteBufUtils.writeUTF8String((ByteBuf)buf, (String)this.playerName);
    }

    public static class Handler
    implements IMessageHandler<PacketRequestPlayerCapability, PacketUpdateCapabilityClient> {
        public PacketUpdateCapabilityClient onMessage(PacketRequestPlayerCapability message, MessageContext ctx) {
            EntityPlayerMP player;
            player = !ctx.getServerHandler().getNetworkManager().isLocalChannel() ? FMLServerHandler.instance().getServer().getPlayerList().getPlayerByUsername(message.playerName) : Minecraft.getMinecraft().getIntegratedServer().getPlayerList().getPlayerByUsername(message.playerName);
            if (player.hasCapability(DofuxiaCore.DOFUS_CAP, null)) {
            	player.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue((double)(65 + ((IDofusCapabilities)player.getCapability(DofuxiaCore.DOFUS_CAP, null)).getLevel() * 5));
                return new PacketUpdateCapabilityClient((IDofusCapabilities)player.getCapability(DofuxiaCore.DOFUS_CAP, null), (EntityPlayer)player);
            }
            return null;
        }
    }

}

