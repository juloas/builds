package fr.Vytalic.dofuxia.core.utils.enums;

public class JobsCapacities {

	public class ALCHIMISTE {

		public static final int ORTIE = 0,
				SAUGE = 20,
				TREFLE_5FEUILLES = 40,
				MENTHE_SAUVAGE = 60,
				ORCHIDEE_FREYESQUE = 80,
				EDELWEISS = 100,
				GRAINE_PANDOUILLE = 120,
				GINSENG = 140,
				BELLADONE = 160,
				MANDRAGOR = 180,
				PERCE_NEIGE = 200,
				SALIKRONE = 200;

	}

	public class PAYSAN {

		public static final int WHEAT = 0,
				ORGE = 20,
				AVOINE = 40,
				HOUBLON = 60,
				LIN = 80,
				SEIGLE = 100,
				RIZ = 100,
				MALT = 120,
				CHANVRE = 140,
				MAIS = 160,
				MILLET = 180,
				FROSTIZ = 200,
				QUISNOA = 200;

	}
	
	public class BUCHERON {
		
		public static final int FRENE = 0,
				CHATAIGNIER = 20,
				NOYER = 40,
				CHENE = 60,
				BOMBOU = 70,
				ERABLE = 80,
				OLIVIOLET = 90,
				IF = 100,
				BAMBOU = 110,
				MERISIER = 120,
				NOISETIER = 130,
				EBENE = 140,
				KALIPTUS = 150,
				CHARME = 160,
				BAMBOU_SOMBRE = 170,
				ORME = 180,
				BAMBOU_SACRE = 190,
				TREMBLE = 200,
				AQUAJOU = 200;
		
	}
	
	public class PECHEUR {

	     public static final int GOUJON = 0,
	                 GREUVETTE = 10,
	                 TRUITE = 20,
	                 CRABE_SOURIMI = 30,
	                 POISSON_CHATON = 40,
	                 POISSON_PANE = 50,
	                 CARPE_IEM = 60,
	                 SARDINE_BRILLANTE = 70,
	                 BROCHET = 80,
	                 KALAMOURE = 90,
	                 ANGUILLE = 100,
	                 DORADE_GRISE = 110,
	                 PERCHE = 120,
	                 RAIE_BLEUE = 130,
	                 LOTTE = 140,
	                 REQUIN_MARTEAU_FAUCILLE = 150,
	                 BAR_RIQUAIN = 160,
	                 MORUE = 170,
	                 TANCHE = 180,
	                 ESPADON = 190,
	                 POISKAILLE = 200,
	                 PATELLE = 200;

	    }

	    public class MINEUR {

	        public static final int FER = 0,
	                 CUIVRE = 20,
	                 BRONZE = 40,
	                 KOBALTE = 60,
	                 MANGANESE = 80,
	                 ETAIN = 100,
	                 SILICATE = 100,
	                 ARGENT = 120,
	                 BAUXITE = 140,
	                 OR = 160, 
	                 DOLOMITE = 180,
	                 OBSIDIENNE = 200,
	                 ECUME_DE_MER = 200;

	    }

}
