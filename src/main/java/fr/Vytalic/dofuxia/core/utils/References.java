package fr.Vytalic.dofuxia.core.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class References {

	public static final String MODID = "dofuxia-core";
	public static final String VERSION = "0.1";
	public static final String NAME = "Dofuxia Core";
	public static final String CLIENT_PROXY = "fr.Vytalic.dofuxia.core.proxys.ClientProxy";
	public static final String COMMON_PROXY = "fr.Vytalic.dofuxia.core.proxys.CommonProxy";
	public static final Logger LOGGER = LogManager.getLogger(MODID);
	
}
