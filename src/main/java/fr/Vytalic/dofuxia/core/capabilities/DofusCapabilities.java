package fr.Vytalic.dofuxia.core.capabilities;

import com.mojang.realmsclient.gui.ChatFormatting;

import fr.Vytalic.dofuxia.core.DofuxiaCore;
import fr.Vytalic.dofuxia.core.network.DofusNetwork;
import fr.Vytalic.dofuxia.core.network.packet.PacketUpdateCapabilityClient;
import fr.Vytalic.dofuxia.core.utils.JobHelper;
import fr.Vytalic.dofuxia.core.utils.PlayerClasses;
import fr.Vytalic.dofuxia.core.utils.enums.DofusJobs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;

public class DofusCapabilities implements ICapabilityProvider, IDofusCapabilities, INBTSerializable<NBTTagCompound> {

	private int level = 1, kamas = 0, agility = 0, luck = 0, intelligence = 0, wisdom = 0, vitality = 0, strength = 0;
	private long xpAlchimiste= 0L, xpBijoutier= 0L, xpBricoleur= 0L, xpBucheron= 0L, xpChasseur= 0L, xpCordomage= 0L, xpCordonnier= 0L, xpCostumage= 0L, xpFacomage= 0L, xpFaconeur= 0L, xpForgemage= 0L, xpForgeron= 0L, xpJoaillomage= 0L, xpMineur= 0L, xpPaysan= 0L, xpPecheur= 0L, xpSculptemage= 0L, xpSculpteur= 0L, xpTailleur = 0L;
	private long xp = 0L;

	private PlayerClasses player_class = PlayerClasses.PLAYER;

	@Override
	public long getXpAlchimiste() {
		return xpAlchimiste;
	}

	@Override
	public void setXpAlchimiste(long xpAlchimiste) {
		this.xpAlchimiste = xpAlchimiste;
	}

	@Override
	public long getXpBijoutier() {
		return xpBijoutier;
	}

	@Override
	public void setXpBijoutier(long xpBijoutier) {
		this.xpBijoutier = xpBijoutier;
	}

	@Override
	public long getXpBricoleur() {
		return xpBricoleur;
	}

	@Override
	public void setXpBricoleur(long xpBricoleur) {
		this.xpBricoleur = xpBricoleur;
	}

	@Override
	public long getXpBucheron() {
		return xpBucheron;
	}

	@Override
	public void setXpBucheron(long xpBucheron) {
		this.xpBucheron = xpBucheron;
	}

	@Override
	public long getXpChasseur() {
		return xpChasseur;
	}

	@Override
	public void setXpChasseur(long xpChasseur) {
		this.xpChasseur = xpChasseur;
	}

	@Override
	public long getXpCordomage() {
		return xpCordomage;
	}

	@Override
	public void setXpCordomage(long xpCordomage) {
		this.xpCordomage = xpCordomage;
	}

	@Override
	public long getXpCordonnier() {
		return xpCordonnier;
	}

	@Override
	public void setXpCordonnier(long xpCordonnier) {
		this.xpCordonnier = xpCordonnier;
	}

	@Override
	public long getXpCostumage() {
		return xpCostumage;
	}

	@Override
	public void setXpCostumage(long xpCostumage) {
		this.xpCostumage = xpCostumage;
	}

	@Override
	public long getXpFacomage() {
		return xpFacomage;
	}

	@Override
	public void setXpFacomage(long xpFacomage) {
		this.xpFacomage = xpFacomage;
	}

	@Override
	public long getXpFaconeur() {
		return xpFaconeur;
	}

	@Override
	public void setXpFaconeur(long xpFaconeur) {
		this.xpFaconeur = xpFaconeur;
	}

	@Override
	public long getXpForgeron() {
		return xpForgeron;
	}

	@Override
	public void setXpForgeron(long xpForgeron) {
		this.xpForgeron = xpForgeron;
	}

	@Override
	public long getXpJoaillomage() {
		return xpJoaillomage;
	}

	@Override
	public void setXpJoaillomage(long xpJoaillomage) {
		this.xpJoaillomage = xpJoaillomage;
	}

	@Override
	public long getXpMineur() {
		return xpMineur;
	}

	@Override
	public void setXpMineur(long xpMineur) {
		this.xpMineur = xpMineur;
	}

	@Override
	public long getXpPaysan() {
		return xpPaysan;
	}

	@Override
	public void setXpPaysan(long xpPaysan) {
		this.xpPaysan = xpPaysan;
	}

	@Override
	public long getXpPecheur() {
		return xpPecheur;
	}

	@Override
	public void setXpPecheur(long xpPecheur) {
		this.xpPecheur = xpPecheur;
	}

	@Override
	public long getXpSculptemage() {
		return xpSculptemage;
	}

	@Override
	public void setXpSculptemage(long xpSculptemage) {
		this.xpSculptemage = xpSculptemage;
	}

	@Override
	public long getXpSculpteur() {
		return xpSculpteur;
	}

	@Override
	public void setXpSculpteur(long xpSculpteur) {
		this.xpSculpteur = xpSculpteur;
	}

	@Override
	public long getXpTailleur() {
		return xpTailleur;
	}

	@Override
	public void setXpTailleur(long xpTailleur) {
		this.xpTailleur = xpTailleur;
	}

	@SuppressWarnings("unused")
	private EntityPlayer player;
	public DofusCapabilities(EntityPlayer player) {  this.player = player; }

	@Override
	public int getStrength() { return strength; }

	@Override
	public void setStrength(int strength) { this.strength = strength; }

	@Override
	public int getAgility() { return agility; }

	@Override
	public void setAgility(int agility) { this.agility = agility; }

	@Override
	public int getLuck() { return luck; }

	@Override
	public void setLuck(int luck) { this.luck = luck; }

	@Override
	public int getIntelligence() { return intelligence; }

	@Override
	public void setIntelligence(int intelligence) { this.intelligence = intelligence; }

	@Override
	public int getWisdom() { return wisdom; }

	@Override
	public void setWisdom(int wisdom) { this.wisdom = wisdom; }

	@Override
	public int getVitality() { return vitality; }

	@Override
	public void setVitality(int vitality) { this.vitality = vitality; }

	@Override
	public int getLevel() { return this.level; }

	@Override
	public void addLevel(int level) { this.level += level; }

	@Override
	public long getXp() { return this.xp; }

	@Override
	public void addXp(long xp) { this.xp += xp; }

	@Override
	public void setXp(long xp) { this.xp = xp; }

	@Override
	public void setLevel(int level) { this.level = level; }

	@Override
	public int getKamas() { return this.kamas; }

	@Override
	public void setKamas(int ata) { this.kamas = ata; }

	@Override
	public void removeKamas(int ata) { this.kamas -= ata; }

	@Override
	public void addKamas(int ata) { this.kamas += ata; }

	@Override
	public NBTBase getTag() {

		NBTTagCompound tag = new NBTTagCompound();

		tag.setInteger("level", this.getLevel());
		tag.setLong("xp", this.getXp());
		tag.setInteger("kamas", this.getKamas());

		tag.setInteger("vitality", this.getVitality());
		tag.setInteger("wisdom", this.getWisdom());
		tag.setInteger("strength", this.getStrength());
		tag.setInteger("intelligence", this.getIntelligence());
		tag.setInteger("luck", this.getLuck());
		tag.setInteger("agility", this.getAgility());

		tag.setInteger("class", PlayerClasses.getInt(player_class));

		NBTTagCompound jobs_tag = new NBTTagCompound();

		jobs_tag.setLong("xp_alchimiste", xpAlchimiste);
		jobs_tag.setLong("xp_bijoutier", xpBijoutier);
		jobs_tag.setLong("xp_bricoleur", xpBricoleur);
		jobs_tag.setLong("xp_bucheron", xpBucheron);
		jobs_tag.setLong("xp_chasseur", xpChasseur);
		jobs_tag.setLong("xp_cordomage", xpCordomage);
		jobs_tag.setLong("xp_cordonnier", xpCordonnier);
		jobs_tag.setLong("xp_costumage", xpCostumage);
		jobs_tag.setLong("xp_facomage", xpFacomage);
		jobs_tag.setLong("xp_faconeur", xpFaconeur);
		jobs_tag.setLong("xp_forgemage", xpForgemage);
		jobs_tag.setLong("xp_forgeron", xpForgeron);
		jobs_tag.setLong("xp_joaillomage", xpJoaillomage);
		jobs_tag.setLong("xp_mineur", xpMineur);
		jobs_tag.setLong("xp_paysan", xpPaysan);
		jobs_tag.setLong("xp_pecheur", xpPecheur);
		jobs_tag.setLong("xp_sculptemage", xpSculptemage);
		jobs_tag.setLong("xp_sculpteur", xpSculpteur);
		jobs_tag.setLong("xp_tailleur", xpTailleur);

		tag.setTag("jobs", jobs_tag);

		return tag;

	}

	@Override
	public void synch(NBTTagCompound tag) {

		if(tag == null || tag.getTag("jobs") == null) {

			return;

		}

		this.setLevel(tag.getInteger("level"));
		this.setXp(tag.getLong("xp"));
		this.setKamas(tag.getInteger("kamas"));

		this.setVitality(tag.getInteger("vitality"));
		this.setWisdom(tag.getInteger("wisdom"));
		this.setStrength(tag.getInteger("strength"));
		this.setIntelligence(tag.getInteger("intelligence"));
		this.setLuck(tag.getInteger("luck"));
		this.setAgility(tag.getInteger("agility"));

		this.setPlayerClass(tag.getInteger("class"));

		NBTTagCompound jobs_tag = (NBTTagCompound) tag.getTag("jobs");

		this.setXpAlchimiste(jobs_tag.getLong("xp_alchimiste"));
		this.setXpBijoutier(jobs_tag.getLong("xp_bijoutier"));
		this.setXpBricoleur(jobs_tag.getLong("xp_bricoleur"));
		this.setXpBucheron(jobs_tag.getLong("xp_bucheron"));
		this.setXpChasseur(jobs_tag.getLong("xp_chasseur"));
		this.setXpCordomage(jobs_tag.getLong("xp_cordomage"));
		this.setXpCordonnier(jobs_tag.getLong("xp_cordonnier"));
		this.setXpCostumage(jobs_tag.getLong("xp_costumage"));
		this.setXpFacomage(jobs_tag.getLong("xp_facomage"));
		this.setXpFaconeur(jobs_tag.getLong("xp_faconeur"));
		this.setXpForgemage(jobs_tag.getLong("xp_forgemage"));
		this.setXpForgeron(jobs_tag.getLong("xp_forgeron"));
		this.setXpJoaillomage(jobs_tag.getLong("xp_joalliomage"));
		this.setXpMineur(jobs_tag.getLong("xp_mineur"));
		this.setXpPaysan(jobs_tag.getLong("xp_paysan"));
		this.setXpPecheur(jobs_tag.getLong("xp_pecheur"));
		this.setXpSculptemage(jobs_tag.getLong("xp_sculptemage"));
		this.setXpSculpteur(jobs_tag.getLong("xp_sculpteur"));
		this.setXpTailleur(jobs_tag.getLong("xp_tailleur"));	

	}


	@Override
	public void synch(IDofusCapabilities oldCap) {

		this.synch((NBTTagCompound) oldCap.getTag());

	}

	@Override
	public void setPlayerClass(PlayerClasses player_class) { this.player_class = player_class; }

	@Override
	public PlayerClasses getPlayerClass() { return this.player_class; }

	@Override
	public void setPlayerClass(int int_class) { this.player_class = PlayerClasses.getPlayerClass(int_class); }

	@Override
	public NBTTagCompound serializeNBT() { return (NBTTagCompound) getTag(); }

	@Override
	public void deserializeNBT(NBTTagCompound nbt) { synch(nbt); }

	@Override
	public void updateClientCap(EntityPlayer player) {

		DofusNetwork.network.sendToAll((IMessage)new PacketUpdateCapabilityClient(this, player));

	}

	public boolean hasCapability(Capability<?> capability, EnumFacing facing) {

		return DofuxiaCore.DOFUS_CAP != null && capability == DofuxiaCore.DOFUS_CAP;

	}


	@SuppressWarnings("unchecked")
	public <T> T getCapability(Capability<T> capability, EnumFacing facing) {

		return (T)(DofuxiaCore.DOFUS_CAP != null && capability == DofuxiaCore.DOFUS_CAP ? this : null);

	}

	@Override
	public void addXpAlchimiste(long xp, EntityPlayer p) {
		String job = "Alchimiste";
		notifyPlayer(p, this.xpAlchimiste, this.xpAlchimiste + xp, job);
		this.xpAlchimiste += xp;

	}

	@Override
	public void addXpBijoutier(long xp, EntityPlayer p) {
		String job = "Bijoutier";
		notifyPlayer(p, this.xpBijoutier, this.xpBijoutier + xp, job);
		this.xpBijoutier += xp;

	}

	@Override
	public void addXpBricoleur(long xp, EntityPlayer p) {
		String job = "Bricoleur";
		notifyPlayer(p, this.xpBricoleur, this.xpBricoleur + xp, job);
		this.xpBricoleur += xp;

	}

	@Override
	public void addXpBucheron(long xp, EntityPlayer p) {
		String job = "Bucheron";
		notifyPlayer(p, this.xpBucheron, this.xpBucheron + xp, job);
		this.xpBucheron += xp;

	}

	@Override
	public void addXpChasseur(long xp, EntityPlayer p) {
		String job = "Chasseur";
		notifyPlayer(p, this.xpChasseur, this.xpChasseur + xp, job);
		this.xpChasseur += xp;

	}

	@Override
	public void addXpCordomage(long xp, EntityPlayer p) {
		String job = "Cordomage";
		notifyPlayer(p, this.xpCordomage, this.xpCordomage + xp, job);
		this.xpCordomage += xp;

	}

	@Override
	public void addXpCordonnier(long xp, EntityPlayer p) {
		String job = "Cordonnier";
		notifyPlayer(p, this.xpCordonnier, this.xpCordonnier + xp, job);
		this.xpCordonnier += xp;

	}

	@Override
	public void addXpCostumage(long xp, EntityPlayer p) {
		String job = "Costumage";
		notifyPlayer(p, this.xpCostumage, this.xpCostumage + xp, job);
		this.xpCostumage += xp;

	}

	@Override
	public void addXpFacomage(long xp, EntityPlayer p) {
		String job = "Facomage";
		notifyPlayer(p, this.xpFacomage, this.xpFacomage + xp, job);
		this.xpFacomage += xp;

	}

	@Override
	public void addXpFaconeur(long xp, EntityPlayer p) {
		String job = "Faconeur";
		notifyPlayer(p, this.xpFaconeur, this.xpFaconeur + xp, job);
		this.xpFaconeur += xp;

	}

	@Override
	public void addXpForgeron(long xp, EntityPlayer p) {
		String job = "Forgeron";
		notifyPlayer(p, this.xpForgeron, this.xpForgeron + xp, job);
		this.xpForgeron += xp;

	}

	@Override
	public void setXpForgemage(long xp) {

		this.xpForgemage = xp;

	}

	@Override
	public void addXpForgemage(long xp, EntityPlayer p) {
		String job = "Forgemage";
		notifyPlayer(p, this.xpForgemage, this.xpForgemage + xp, job);
		this.xpForgemage = xp;

	}

	@Override
	public long getXpForgemage() {

		return xpForgemage;

	}

	@Override
	public void addXpJoaillomage(long xp, EntityPlayer p) {
		String job = "Joaillomage";
		notifyPlayer(p, this.xpJoaillomage, this.xpJoaillomage + xp, job);
		this.xpJoaillomage += xp;

	}

	@Override
	public void addXpMineur(long xp, EntityPlayer p) {
		String job = "Mineur";
		notifyPlayer(p, this.xpMineur, this.xpMineur + xp, job);
		this.xpMineur += xp;

	}

	@Override
	public void addXpPaysan(long xp, EntityPlayer p) {

		String job = "Paysan";
		notifyPlayer(p, this.xpPaysan, this.xpPaysan + xp, job);

		this.xpPaysan += xp;

	}

	@Override
	public void addXpPecheur(long xp, EntityPlayer p) {

		String job = "Pecheur";
		notifyPlayer(p, this.xpPecheur, this.xpPecheur + xp, job);

		this.xpPecheur += xp;

	}

	@Override
	public void addXpSculptemage(long xp, EntityPlayer p) {

		String job = "Sculptemage";
		notifyPlayer(p, this.xpSculptemage, this.xpSculptemage + xp, job);
		this.xpSculptemage += xp;

	}

	@Override
	public void addXpSculpteur(long xp, EntityPlayer p) {

		String job = "Sculpteur";
		notifyPlayer(p, this.xpSculpteur, this.xpSculpteur + xp, job);

		this.xpSculpteur += xp;

	}

	@Override
	public void addXpTailleur(long xp, EntityPlayer p) {


		String job = "Tailleur";
		notifyPlayer(p, this.xpTailleur, this.xpTailleur + xp, job);

		this.xpTailleur += xp;

	}

	private void notifyPlayer(EntityPlayer p, long oldXp, long actual, String job) {

		if(JobHelper.xpToLevel(oldXp) < JobHelper.xpToLevel(actual)) {

			long last = System.currentTimeMillis();

			p.sendStatusMessage((ITextComponent)new TextComponentString(ChatFormatting.GRAY + "[  ("+ ChatFormatting.GREEN + "+" + ") " + ChatFormatting.GOLD + " Metier Level Up !" + ChatFormatting.GRAY + "  ]"), true);


			Thread async = new Thread(new Runnable() {

				public void run() {

					while(System.currentTimeMillis() - last <= 1420) {}
					p.sendStatusMessage((ITextComponent)new TextComponentString(ChatFormatting.GRAY + "[  " + ChatFormatting.BLUE + job + ChatFormatting.GRAY + " Niveau : " + ChatFormatting.GOLD + JobHelper.xpToLevel(actual) + ChatFormatting.GRAY + "  ]"), true);

				}

			});
			async.start();

		}

	}
	
	@Override
	public void addXpJob(int xpDrop, EntityPlayer playerIn, DofusJobs rt) {
		
		switch(rt) {
		case ALCHIMISTE:
			this.addXpAlchimiste(xpDrop, playerIn);
			break;
		case BIJOUTIER:
			this.addXpBijoutier(xpDrop, playerIn);
			break;
		case BRICOLEUR:
			this.addXpBricoleur(xpDrop, playerIn);
			break;
		case BUCHERON:
			this.addXpBucheron(xpDrop, playerIn);
			break;
		case CHASSEUR:
			this.addXpChasseur(xpDrop, playerIn);
			break;
		case CORDOMAGE:
			this.addXpCordomage(xpDrop, playerIn);
			break;
		case CORDONIER:
			this.addXpCordonnier(xpDrop, playerIn);
			break;
		case COSTUMAGE:
			this.addXpCostumage(xpDrop, playerIn);
			break;
		case FACOMAGE:
			this.addXpFacomage(xpDrop, playerIn);
			break;
		case FACONEUR:
			this.addXpFaconeur(xpDrop, playerIn);
			break;
		case FORGEMAGE:
			this.addXpForgemage(xpDrop, playerIn);
			break;
		case FORGERON:
			this.addXpForgeron(xpDrop, playerIn);
			break;
		case JOALLIOMAGE:
			this.addXpJoaillomage(xpDrop, playerIn);
			break;
		case MINEUR:
			this.addXpMineur(xpDrop, playerIn);
			break;
		case PAYSAN:
			this.addXpPaysan(xpDrop, playerIn);
			break;
		case PECHEUR:
			this.addXpPecheur(xpDrop, playerIn);
			break;
		case SCULPTEMAGE:
			this.addXpSculptemage(xpDrop, playerIn);
			break;
		case SCULPTEUR:
			this.addXpSculpteur(xpDrop, playerIn);
			break;
		case TAILLEUR:
			this.addXpTailleur(xpDrop, playerIn);
			break;
		default:
			break;
		
		}
		
	}

	@Override
	public long getXpJob(DofusJobs rt) {
		
		switch(rt) {
		case ALCHIMISTE:
			return xpAlchimiste;
		case BIJOUTIER:
			return xpBijoutier;
		case BRICOLEUR:
			return xpBricoleur;
		case BUCHERON:
			return xpBucheron;
		case CHASSEUR:
			return xpChasseur;
		case CORDOMAGE:
			return xpCordomage;
		case CORDONIER:
			return xpCordonnier;
		case COSTUMAGE:
			return xpCostumage;
		case FACOMAGE:
			return xpFacomage;
		case FACONEUR:
			return xpFaconeur;
		case FORGEMAGE:
			return xpForgemage;
		case FORGERON:
			return xpForgeron;
		case JOALLIOMAGE:
			return xpJoaillomage;
		case MINEUR:
			return xpMineur;
		case PAYSAN:
			return xpPaysan;
		case PECHEUR:
			return xpPecheur;
		case SCULPTEMAGE:
			return xpSculptemage;
		case SCULPTEUR:
			return xpSculpteur;
		case TAILLEUR:
			return xpTailleur;
		default:
			return xpPaysan;
		
		}
		
	}

}

