package fr.Vytalic.dofuxia.core.capabilities;

import net.minecraft.entity.player.EntityPlayer;

public interface IDofusCapabilities {
    public void updateClientCap(EntityPlayer var1);

    public int getLevel();

    public void addLevel(int var1);

    public void setLevel(int var1);

    public long getXp();

    public void addXp(long var1);

    public void setXp(long var1);

    public int getKamas();

    public void setKamas(int var1);

    public void removeKamas(int var1);

    public void addKamas(int var1);
}

