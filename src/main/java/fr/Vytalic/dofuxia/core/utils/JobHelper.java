package fr.Vytalic.dofuxia.core.utils;

public class JobHelper {
	
	public static int xpToLevel(long xp) {

		/**if(xp == 0) {
			
			return 1;
			
		}
		
		int level;
		boolean loop = true;
		for(level = 200; loop; level--) {
			
			System.out.println(xp + "/" + level + "/(" + level + "-1)");
			
			if(level > 1) {
				
				if(xp/level/(level-1) >= 10) loop = false;
				
			} else if(xp/level/level >= 10) loop = false;
			
		}
		
		
		return level; **/
		
		int lastValidatedLevel = 1;
			
		for(int level = 1; level <= 200; level++) {
				
			if(xp >= level*(level-1)*10) {
				
				lastValidatedLevel = level;
				
			}
				
		}

		return lastValidatedLevel;
		
	}

}
