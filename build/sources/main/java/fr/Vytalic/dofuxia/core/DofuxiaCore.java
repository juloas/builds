package fr.Vytalic.dofuxia.core;

import fr.Vytalic.dofuxia.core.capabilities.DofusCapabilities;
import fr.Vytalic.dofuxia.core.capabilities.DofusCapabilitiesStorage;
import fr.Vytalic.dofuxia.core.capabilities.IDofusCapabilities;
import fr.Vytalic.dofuxia.core.items.DJItems;
import fr.Vytalic.dofuxia.core.network.DofusNetwork;
import fr.Vytalic.dofuxia.core.proxys.CommonProxy;
import fr.Vytalic.dofuxia.core.utils.References;
import net.minecraft.world.GameRules;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartedEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.fml.server.FMLServerHandler;

@Mod(modid = References.MODID, name = References.NAME, version = References.VERSION)
public class DofuxiaCore {

	@SidedProxy(clientSide = References.CLIENT_PROXY, serverSide = References.COMMON_PROXY)
	public static CommonProxy proxy;

	@Mod.Instance
	public static DofuxiaCore instance;

	@CapabilityInject(IDofusCapabilities.class)
	public static final Capability<IDofusCapabilities> DOFUS_CAP = null;

	@Mod.EventHandler
	public void preInit(FMLPreInitializationEvent event) {

		CapabilityManager.INSTANCE.register(IDofusCapabilities.class, new DofusCapabilitiesStorage(), DofusCapabilities.class);
		References.LOGGER.info("Dofuxia Module : " + References.NAME + " pre init !");
		DJItems.init();
		proxy.preInit(event);

	}

	@Mod.EventHandler
	public void init(FMLInitializationEvent event) {
		
		References.LOGGER.info("Dofuxia Module : " + References.NAME + " init !");
		DofusNetwork.init();
		proxy.init(event);

	}

	@Mod.EventHandler
	public void postInit(FMLPostInitializationEvent event) {

		References.LOGGER.info("Dofuxia Module : " + References.NAME + " post init !");
		proxy.postInit(event);

	}
	
	@Mod.EventHandler
	@SideOnly(Side.SERVER)
    public void onServerStarted(FMLServerStartedEvent event) {
		
       if(event.getSide() == Side.SERVER) {
    	   
    	   GameRules gamerules = FMLServerHandler.instance().getServer().worlds[0].getGameRules();
           
           gamerules.setOrCreateGameRule("keepInventory", "true");
           gamerules.setOrCreateGameRule("doFireTick", "false");
           gamerules.setOrCreateGameRule("mobGriefing", "false");
           gamerules.setOrCreateGameRule("showDeathMessages", "false");
           gamerules.setOrCreateGameRule("naturalRegeneration", "false");
           
           References.LOGGER.info("Gamer Rules");
           References.LOGGER.info("------------------------");
           References.LOGGER.info("keepInventory : " + gamerules.getBoolean("keepInventory") + " / default: true");
           References.LOGGER.info("doFireTick : " + gamerules.getBoolean("doFireTick") + " / default: false");
           References.LOGGER.info("mobGriefing : " + gamerules.getBoolean("mobGriefing") + " / default: false");
           References.LOGGER.info("showDeathMessages : " + gamerules.getBoolean("showDeathMessages") + " / default: false");
           References.LOGGER.info("naturalRegeneration : " + gamerules.getBoolean("naturalRegeneration") + " / default: false");
           References.LOGGER.info("------------------------");
    	   
       }
        
    }


}
