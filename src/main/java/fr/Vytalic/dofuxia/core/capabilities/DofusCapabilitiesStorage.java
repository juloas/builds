package fr.Vytalic.dofuxia.core.capabilities;

import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;

public class DofusCapabilitiesStorage implements Capability.IStorage<IDofusCapabilities> {
    
	public NBTBase writeNBT(Capability<IDofusCapabilities> capability, IDofusCapabilities instance, EnumFacing side) {
       
        return instance.getTag();
        
    }

    public void readNBT(Capability<IDofusCapabilities> capability, IDofusCapabilities instance, EnumFacing side, NBTBase nbt) {
       
    	NBTTagCompound tag = (NBTTagCompound)nbt;
        instance.synch(tag);
        
    }
    
}

