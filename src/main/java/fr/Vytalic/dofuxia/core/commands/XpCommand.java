package fr.Vytalic.dofuxia.core.commands;

import fr.Vytalic.dofuxia.core.utils.XpHelper;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;

public class XpCommand extends CommandBase {

	@Override
	public String getName() {
		
		return "dcxp";
		
	}

	@Override
	public String getUsage(ICommandSender sender) {
		
		return "commands.dcxp.usage";
		
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
		
		if(sender.canUseCommand(server.getOpPermissionLevel(), "dcxp")) {
			
			if(args[0] != null && args[1] != null && args[2] != null) {
				
				EntityPlayerMP player = server.getPlayerList().getPlayerByUsername(args[0]);
				int value = Integer.parseInt(args[2]);
				
				if(args[1].equalsIgnoreCase("add")) {
					
					XpHelper.addXp(player, value);
					
				} else if(args[1].equalsIgnoreCase("remove")) {
					
					XpHelper.addXp(player, -value);
					
				}
				
			}
			
		}
		
	}
	
	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
		
		if(sender instanceof EntityPlayer) {
			
			if(((EntityPlayer)sender).capabilities.isCreativeMode) {
				
				return true;
				
			} else {
				
				return false;
				
			}
			
		} else {
			
			return true;
			
		}
		
	}

}
