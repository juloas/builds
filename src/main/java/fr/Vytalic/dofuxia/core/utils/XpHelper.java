/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  com.mojang.realmsclient.gui.ChatFormatting
 *  net.minecraft.entity.player.EntityPlayer
 *  net.minecraft.entity.player.EntityPlayerMP
 *  net.minecraft.util.EnumFacing
 *  net.minecraft.util.text.ITextComponent
 *  net.minecraft.util.text.TextComponentString
 *  net.minecraftforge.common.capabilities.Capability
 */
package fr.Vytalic.dofuxia.core.utils;

import com.mojang.realmsclient.gui.ChatFormatting;

import fr.Vytalic.dofuxia.core.DofuxiaCore;
import fr.Vytalic.dofuxia.core.capabilities.IDofusCapabilities;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;

public class XpHelper {
    public static long[] up = new long[]{0L, 110L, 650L, 1500L, 2800L, 4800L, 7300L, 10500L, 14500L, 19200L, 25200L, 32600L, 41000L, 50500L, 61000L, 75000L, 91000L, 115000L, 142000L, 171000L, 202000L, 235000L, 270000L, 310000L, 353000L, 398500L, 448000L, 503000L, 561000L, 621000L, 687000L, 755000L, 829000L, 910000L, 1000000L, 1100000L, 1240000L, 1400000L, 1580000L, 1780000L, 2000000L, 2250000L, 2530000L, 2850000L, 3200000L, 3570000L, 3960000L, 4400000L, 4860000L, 5350000L, 5860000L, 6390000L, 6950000L, 7530000L, 8130000L, 8765100L, 9420000L, 10150000L, 10894000L, 11655000L, 12450000L, 13280000L, 14130000L, 15170000L, 16251000L, 17377000L, 18553000L, 19778000L, 21055000L, 22385000L, 23529000L, 25209000L, 26707000L, 28264000L, 29882000L, 31563000L, 33307000L, 35118000L, 36997000L, 38945000L, 40965000L, 43059000L, 45229000L, 47476000L, 49803000L, 52211000L, 54704000L, 57284000L, 59952000L, 62712000L, 65565000L, 68514000L, 71561000L, 74710000L, 77963000L, 81323000L, 84792000L, 88374000L, 92071000L, 95886000L, 99823000L, 103885000L, 108075000L, 112396000L, 116853000L, 121447000L, 126184000L, 131066000L, 136098000L, 141283000L, 146626000L, 152130000L, 157800000L, 163640000L, 169655000L, 175848000L, 182225000L, 188791000L, 195550000L, 202507000L, 209667000L, 217037000L, 224620000L, 232424000L, 240452000L, 248712000L, 257209000L, 265949000L, 274939000L, 284186000L, 293694000L, 303473000L, 313527000L, 323866000L, 334495000L, 345423000L, 356657000L, 368206000L, 380076000L, 392278000L, 404818000L, 417706000L, 430952000L, 444564000L, 458551000L, 472924000L, 487693000L, 502867000L, 518458000L, 534476000L, 551000000L, 567839000L, 585206000L, 603047000L, 621374000L, 640199000L, 659536000L, 679398000L, 699798000L, 720751000L, 742272000L, 764374000L, 787074000L, 810387000L, 834329000L, 858917000L, 884167000L, 910098000L, 936727000L, 964073000L, 992154000L, 1020991000L, 1050603000L, 1081010000L, 1112235000L, 1144298000L, 1177222000L, 1211030000L, 1245745000L, 1281393000L, 1317997000L, 1355584000L, 1404179000L, 1463811000L, 1534506000L, 1616294000L, 1709205000L, 1813267000L, 1928513000L, 2054975000L, 2192686000L, 2341679000L, 2501990000L, 2673655000L, 2856710000L, 3051194000L, 3257146000L, 3474606000L, 3703616000L, 7407232000L, 2147483647L};

    
    public static void addXp(EntityPlayerMP player, long xp) {
    	
        ((IDofusCapabilities)player.getCapability(DofuxiaCore.DOFUS_CAP, null)).addXp(xp);
        XpHelper.helpUp(player);
        
    }

    public static void helpUp(EntityPlayerMP p) {
        IDofusCapabilities capa = (IDofusCapabilities)p.getCapability(DofuxiaCore.DOFUS_CAP, null);
        switch (capa.getLevel()) {
            case 1: {
                if (capa.getXp() < up[1]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 2: {
                if (capa.getXp() < up[2]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 3: {
                if (capa.getXp() < up[3]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 4: {
                if (capa.getXp() < up[4]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 5: {
                if (capa.getXp() < up[5]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 6: {
                if (capa.getXp() < up[6]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 7: {
                if (capa.getXp() < up[7]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 8: {
                if (capa.getXp() < up[8]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 9: {
                if (capa.getXp() < up[9]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 10: {
                if (capa.getXp() < up[10]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 11: {
                if (capa.getXp() < up[11]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 12: {
                if (capa.getXp() < up[12]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 13: {
                if (capa.getXp() < up[13]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 14: {
                if (capa.getXp() < up[14]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 15: {
                if (capa.getXp() < up[15]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 16: {
                if (capa.getXp() < up[16]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 17: {
                if (capa.getXp() < up[17]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 18: {
                if (capa.getXp() < up[18]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 19: {
                if (capa.getXp() < up[19]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 20: {
                if (capa.getXp() < up[20]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 21: {
                if (capa.getXp() < up[21]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 22: {
                if (capa.getXp() < up[22]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 23: {
                if (capa.getXp() < up[23]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 24: {
                if (capa.getXp() < up[24]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 25: {
                if (capa.getXp() < up[25]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 26: {
                if (capa.getXp() < up[26]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 27: {
                if (capa.getXp() < up[27]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 28: {
                if (capa.getXp() < up[28]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 29: {
                if (capa.getXp() < up[29]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 30: {
                if (capa.getXp() < up[30]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 31: {
                if (capa.getXp() < up[31]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 32: {
                if (capa.getXp() < up[32]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 33: {
                if (capa.getXp() < up[33]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 34: {
                if (capa.getXp() < up[34]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 35: {
                if (capa.getXp() < up[35]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 36: {
                if (capa.getXp() < up[36]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 37: {
                if (capa.getXp() < up[37]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 38: {
                if (capa.getXp() < up[38]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 39: {
                if (capa.getXp() < up[39]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 40: {
                if (capa.getXp() < up[40]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 41: {
                if (capa.getXp() < up[41]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 42: {
                if (capa.getXp() < up[42]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 43: {
                if (capa.getXp() < up[43]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 44: {
                if (capa.getXp() < up[44]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 45: {
                if (capa.getXp() < up[45]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 46: {
                if (capa.getXp() < up[46]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 47: {
                if (capa.getXp() < up[47]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 48: {
                if (capa.getXp() < up[48]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 49: {
                if (capa.getXp() < up[49]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 50: {
                if (capa.getXp() < up[50]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 51: {
                if (capa.getXp() < up[51]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 52: {
                if (capa.getXp() < up[52]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 53: {
                if (capa.getXp() < up[53]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 54: {
                if (capa.getXp() < up[54]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 55: {
                if (capa.getXp() < up[55]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 56: {
                if (capa.getXp() < up[56]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 57: {
                if (capa.getXp() < up[57]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 58: {
                if (capa.getXp() < up[58]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 59: {
                if (capa.getXp() < up[59]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 60: {
                if (capa.getXp() < up[60]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 61: {
                if (capa.getXp() < up[61]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 62: {
                if (capa.getXp() < up[62]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 63: {
                if (capa.getXp() < up[63]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 64: {
                if (capa.getXp() < up[64]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 65: {
                if (capa.getXp() < up[65]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 66: {
                if (capa.getXp() < up[66]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 67: {
                if (capa.getXp() < up[67]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 68: {
                if (capa.getXp() < up[68]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 69: {
                if (capa.getXp() < up[69]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 70: {
                if (capa.getXp() < up[70]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 71: {
                if (capa.getXp() < up[71]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 72: {
                if (capa.getXp() < up[72]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 73: {
                if (capa.getXp() < up[73]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 74: {
                if (capa.getXp() < up[74]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 75: {
                if (capa.getXp() < up[75]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 76: {
                if (capa.getXp() < up[76]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 77: {
                if (capa.getXp() < up[77]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 78: {
                if (capa.getXp() < up[78]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 79: {
                if (capa.getXp() < up[79]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 80: {
                if (capa.getXp() < up[80]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 81: {
                if (capa.getXp() < up[81]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 82: {
                if (capa.getXp() < up[82]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 83: {
                if (capa.getXp() < up[83]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 84: {
                if (capa.getXp() < up[84]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 85: {
                if (capa.getXp() < up[85]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 86: {
                if (capa.getXp() < up[86]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 87: {
                if (capa.getXp() < up[87]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 88: {
                if (capa.getXp() < up[88]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 89: {
                if (capa.getXp() < up[89]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 90: {
                if (capa.getXp() < up[90]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 91: {
                if (capa.getXp() < up[91]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 92: {
                if (capa.getXp() < up[92]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 93: {
                if (capa.getXp() < up[93]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 94: {
                if (capa.getXp() < up[94]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 95: {
                if (capa.getXp() < up[95]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 96: {
                if (capa.getXp() < up[96]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 97: {
                if (capa.getXp() < up[97]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 98: {
                if (capa.getXp() < up[98]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 99: {
                if (capa.getXp() < up[99]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 100: {
                if (capa.getXp() < up[100]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 101: {
                if (capa.getXp() < up[101]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 102: {
                if (capa.getXp() < up[102]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 103: {
                if (capa.getXp() < up[103]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 104: {
                if (capa.getXp() < up[104]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 105: {
                if (capa.getXp() < up[105]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 106: {
                if (capa.getXp() < up[106]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 107: {
                if (capa.getXp() < up[107]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 108: {
                if (capa.getXp() < up[108]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 109: {
                if (capa.getXp() < up[109]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 110: {
                if (capa.getXp() < up[110]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 111: {
                if (capa.getXp() < up[111]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 112: {
                if (capa.getXp() < up[112]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 113: {
                if (capa.getXp() < up[113]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 114: {
                if (capa.getXp() < up[114]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 115: {
                if (capa.getXp() < up[115]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 116: {
                if (capa.getXp() < up[116]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 117: {
                if (capa.getXp() < up[117]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 118: {
                if (capa.getXp() < up[118]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 119: {
                if (capa.getXp() < up[119]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 120: {
                if (capa.getXp() < up[120]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 121: {
                if (capa.getXp() < up[121]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 122: {
                if (capa.getXp() < up[122]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 123: {
                if (capa.getXp() < up[123]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 124: {
                if (capa.getXp() < up[124]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 125: {
                if (capa.getXp() < up[125]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 126: {
                if (capa.getXp() < up[126]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 127: {
                if (capa.getXp() < up[127]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 128: {
                if (capa.getXp() < up[128]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 129: {
                if (capa.getXp() < up[129]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 130: {
                if (capa.getXp() < up[130]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 131: {
                if (capa.getXp() < up[131]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 132: {
                if (capa.getXp() < up[132]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 133: {
                if (capa.getXp() < up[133]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 134: {
                if (capa.getXp() < up[134]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 135: {
                if (capa.getXp() < up[135]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 136: {
                if (capa.getXp() < up[136]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 137: {
                if (capa.getXp() < up[137]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 138: {
                if (capa.getXp() < up[138]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 139: {
                if (capa.getXp() < up[139]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 140: {
                if (capa.getXp() < up[140]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 141: {
                if (capa.getXp() < up[141]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 142: {
                if (capa.getXp() < up[142]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 143: {
                if (capa.getXp() < up[143]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 144: {
                if (capa.getXp() < up[144]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 145: {
                if (capa.getXp() < up[145]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 146: {
                if (capa.getXp() < up[146]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 147: {
                if (capa.getXp() < up[147]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 148: {
                if (capa.getXp() < up[148]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 149: {
                if (capa.getXp() < up[149]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 150: {
                if (capa.getXp() < up[150]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 151: {
                if (capa.getXp() < up[151]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 152: {
                if (capa.getXp() < up[152]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 153: {
                if (capa.getXp() < up[153]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 154: {
                if (capa.getXp() < up[154]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 155: {
                if (capa.getXp() < up[155]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 156: {
                if (capa.getXp() < up[156]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 157: {
                if (capa.getXp() < up[157]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 158: {
                if (capa.getXp() < up[158]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 159: {
                if (capa.getXp() < up[159]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 160: {
                if (capa.getXp() < up[160]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 161: {
                if (capa.getXp() < up[161]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 162: {
                if (capa.getXp() < up[162]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 163: {
                if (capa.getXp() < up[163]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 164: {
                if (capa.getXp() < up[164]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 165: {
                if (capa.getXp() < up[165]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 166: {
                if (capa.getXp() < up[166]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 167: {
                if (capa.getXp() < up[167]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 168: {
                if (capa.getXp() < up[168]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 169: {
                if (capa.getXp() < up[169]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 170: {
                if (capa.getXp() < up[170]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 171: {
                if (capa.getXp() < up[171]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 172: {
                if (capa.getXp() < up[172]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 173: {
                if (capa.getXp() < up[173]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 174: {
                if (capa.getXp() < up[174]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 175: {
                if (capa.getXp() < up[175]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 176: {
                if (capa.getXp() < up[176]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 177: {
                if (capa.getXp() < up[177]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 178: {
                if (capa.getXp() < up[178]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 179: {
                if (capa.getXp() < up[179]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 180: {
                if (capa.getXp() < up[180]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 181: {
                if (capa.getXp() < up[181]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 182: {
                if (capa.getXp() < up[182]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 183: {
                if (capa.getXp() < up[183]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 184: {
                if (capa.getXp() < up[184]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 185: {
                if (capa.getXp() < up[185]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 186: {
                if (capa.getXp() < up[186]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 187: {
                if (capa.getXp() < up[187]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 188: {
                if (capa.getXp() < up[188]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 189: {
                if (capa.getXp() < up[189]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 190: {
                if (capa.getXp() < up[190]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 191: {
                if (capa.getXp() < up[191]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 192: {
                if (capa.getXp() < up[192]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 193: {
                if (capa.getXp() < up[193]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 194: {
                if (capa.getXp() < up[194]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 195: {
                if (capa.getXp() < up[195]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 196: {
                if (capa.getXp() < up[196]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 197: {
                if (capa.getXp() < up[197]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 198: {
                if (capa.getXp() < up[198]) break;
                XpHelper.playerUp(p);
                break;
            }
            case 199: {
                if (capa.getXp() < up[199]) break;
                XpHelper.playerUp(p);    
            }
            
            case 200: {
            	
            	break;
            	
            }
            
        }
        
    }

    public static void playerUp(EntityPlayerMP p) {
    	
        IDofusCapabilities capa = (IDofusCapabilities)p.getCapability(DofuxiaCore.DOFUS_CAP, null);
        capa.addLevel(1);
        capa.updateClientCap((EntityPlayer)p);
        
        long last = System.currentTimeMillis();
        
        
        Thread async = new Thread(new Runnable() {
			
			public void run() {
				
				while(System.currentTimeMillis() - last <= 3420) {}
				 p.sendStatusMessage((ITextComponent)new TextComponentString(ChatFormatting.GRAY + "[  ("+ ChatFormatting.GREEN + "+" + ") " + ChatFormatting.GOLD + " Level Up !" + ChatFormatting.GRAY + "  ]"), true);
			     
			}
			
		});
        
		async.start();
       
        async = new Thread(new Runnable() {
			
			public void run() {
				
				while(System.currentTimeMillis() - last <= 4420) {}
				p.sendStatusMessage((ITextComponent)new TextComponentString(ChatFormatting.GRAY + "[  " + ChatFormatting.BLUE + "Level : " + capa.getLevel() + ChatFormatting.GRAY + "  ]"), true);
				
			}
			
		});
		async.start();
        XpHelper.helpUp(p);
        
    }
    
}

