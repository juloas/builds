package fr.Vytalic.dofuxia.core.capabilities;

import fr.Vytalic.dofuxia.core.DofuxiaCore;
import fr.Vytalic.dofuxia.core.network.DofusNetwork;
import fr.Vytalic.dofuxia.core.network.packet.PacketRequestPlayerCapability;
import fr.Vytalic.dofuxia.core.network.packet.PacketUpdateCapabilityClient;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;

@Mod.EventBusSubscriber(modid="dofucraft")
public class DofusCapabilitiesEvent {
	@SubscribeEvent
	public static void onAttachCapability(AttachCapabilitiesEvent<Entity> event) {
		
		if (event.getObject() instanceof EntityPlayer) {
			
			event.addCapability(new ResourceLocation("dofucraft", "dofus_cap"), (ICapabilityProvider)new DofusCapabilities((EntityPlayer)event.getObject()));
		
		}
	}

	@SubscribeEvent
	public static void onPlayerCloned(net.minecraftforge.event.entity.player.PlayerEvent.Clone event) {

		if (event.getOriginal().hasCapability(DofuxiaCore.DOFUS_CAP, null)) {

			IDofusCapabilities originalCapa = (IDofusCapabilities)event.getOriginal().getCapability(DofuxiaCore.DOFUS_CAP, null);
			IDofusCapabilities newCap = (IDofusCapabilities)event.getEntityPlayer().getCapability(DofuxiaCore.DOFUS_CAP, null);

			newCap.setLevel(originalCapa.getLevel());
			newCap.setXp(originalCapa.getXp());
			newCap.setKamas(originalCapa.getKamas());

			if (!event.isWasDeath()) {

				event.getEntityPlayer().setHealth(event.getOriginal().getHealth());

			}

		}

	}

	@SubscribeEvent
	public static void onPlayerChangeDimension(net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerChangedDimensionEvent event) {

		if (!event.player.world.isRemote && event.player.hasCapability(DofuxiaCore.DOFUS_CAP, null)) {

			DofusNetwork.network.sendTo((IMessage)new PacketUpdateCapabilityClient(event.player), (EntityPlayerMP)event.player);

		}
	}

	@SubscribeEvent
	public static void syncCapabilities(TickEvent.PlayerTickEvent event) {

		if (!event.player.isDead && event.player.world.isRemote && event.player.world.getTotalWorldTime() % 42L == 0L) {

			DofusNetwork.network.sendToServer((IMessage)new PacketRequestPlayerCapability(event.player.getName()));

		}
	}

}
