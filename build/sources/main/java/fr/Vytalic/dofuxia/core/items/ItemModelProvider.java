package fr.Vytalic.dofuxia.core.items;

import net.minecraft.item.Item;

public interface ItemModelProvider {
	
	 public abstract void registerItemModel(Item paramItem);

}
