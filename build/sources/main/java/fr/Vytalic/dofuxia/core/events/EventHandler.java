package fr.Vytalic.dofuxia.core.events;

import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;

@Mod.EventBusSubscriber(modid="dofuxia-core", value={Side.CLIENT})
public class EventHandler {
	
    @SubscribeEvent
    public static void renderGameOverlayEvent(RenderGameOverlayEvent e) {
       
    	if (e.getType().equals((Object)RenderGameOverlayEvent.ElementType.HEALTH) || e.getType().equals((Object)RenderGameOverlayEvent.ElementType.EXPERIENCE) || e.getType().equals((Object)RenderGameOverlayEvent.ElementType.FOOD)) {
           
    		e.setCanceled(true);
        
    	}
    	
    }
    
}

