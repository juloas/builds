/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  net.minecraft.entity.Entity
 *  net.minecraft.entity.item.EntityItem
 *  net.minecraft.entity.player.EntityPlayer
 *  net.minecraft.entity.player.PlayerCapabilities
 *  net.minecraft.item.ItemStack
 *  net.minecraft.nbt.NBTTagCompound
 *  net.minecraft.util.DamageSource
 *  net.minecraft.world.World
 *  net.minecraftforge.event.entity.living.LivingDropsEvent
 *  net.minecraftforge.event.entity.player.EntityItemPickupEvent
 *  net.minecraftforge.fml.common.Mod
 *  net.minecraftforge.fml.common.Mod$EventBusSubscriber
 *  net.minecraftforge.fml.common.eventhandler.SubscribeEvent
 *  net.minecraftforge.fml.common.gameevent.TickEvent
 *  net.minecraftforge.fml.common.gameevent.TickEvent$PlayerTickEvent
 */
package fr.Vytalic.dofuxia.core.events;

import java.util.List;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.PlayerCapabilities;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.DamageSource;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.living.LivingDropsEvent;
import net.minecraftforge.event.entity.player.EntityItemPickupEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;

@Mod.EventBusSubscriber(modid="dofuxia-core")
public class DofusPlayerEvent {
	
    @SubscribeEvent
    public static void playerTickEvent(TickEvent.PlayerTickEvent e) {
    	
        EntityPlayer player = e.player;
        World world = player.world;
        
        if (world.getTotalWorldTime() % 20L == 0L) {
        	
            player.heal(1.0f);
            
        }
        
    }

    @SubscribeEvent
    public static void onDropPossible(EntityItemPickupEvent event) {
    	
        if (!event.getEntity().world.isRemote) {
        	
            if (event.getItem().getEntityItem().hasTagCompound()) {
            	
                if (event.getEntityPlayer().getName().equalsIgnoreCase(event.getItem().getEntityItem().getTagCompound().getString("killer"))) {
                	
                    event.setCanceled(false);
                    event.getItem().getEntityItem().setTagCompound(null);
                    
                } else if (event.getEntityPlayer().capabilities.isCreativeMode) {
                	
                    event.setCanceled(false);
                    event.getItem().getEntityItem().setTagCompound(null);
                    
                } else {
                	
                    event.setCanceled(true);
                    
                }
                
            }
            
        } else {
        	
            event.setCanceled(false);
            
        }
        
    }

    @SubscribeEvent
    public static void onDropPossible(LivingDropsEvent event) {
    	
        List<EntityItem> drops = event.getDrops();
        
        for (Object o : drops) {
        	
            if (!(o instanceof EntityItem) || !(event.getSource().getSourceOfDamage() instanceof EntityPlayer)) continue;
            EntityItem item = (EntityItem)o;
            EntityPlayer player = (EntityPlayer)event.getSource().getSourceOfDamage();
            NBTTagCompound tag = new NBTTagCompound();
            tag.setString("killer", player.getName());
            item.getEntityItem().setTagCompound(tag);
            
        }
        
    }
    
}

