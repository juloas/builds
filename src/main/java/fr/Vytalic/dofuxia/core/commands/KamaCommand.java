package fr.Vytalic.dofuxia.core.commands;

import fr.Vytalic.dofuxia.core.utils.KamaHelper;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;

public class KamaCommand extends CommandBase {

	@Override
	public String getName() {
		
		return "dckama";
		
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
		
		if(sender.canUseCommand(server.getOpPermissionLevel(), "dckama")) {
			
			if(args[0] != null && args[1] != null && args[2] != null) {
				
				EntityPlayerMP player = server.getPlayerList().getPlayerByUsername(args[0]);
				int value = Integer.parseInt(args[2]);
				
				if(args[1].equalsIgnoreCase("add")) {
					
					KamaHelper.addKama(player, value);
					
				} else if(args[1].equalsIgnoreCase("remove")) {
					
					KamaHelper.removeKama(player, value);
					
				}
				
			}
			
		}
		
	}

	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
		
		if(sender instanceof EntityPlayer) {
			
			if(((EntityPlayer)sender).capabilities.isCreativeMode) {
				
				return true;
				
			} else {
				
				return false;
				
			}
			
		} else {
			
			return true;
			
		}
		
	}

	@Override
	public String getUsage(ICommandSender sender) {
		
		return "/dckama";
	}

}
