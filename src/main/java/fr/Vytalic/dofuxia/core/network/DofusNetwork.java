package fr.Vytalic.dofuxia.core.network;

import fr.Vytalic.dofuxia.core.network.packet.PacketRequestPlayerCapability;
import fr.Vytalic.dofuxia.core.network.packet.PacketUpdateCapabilityClient;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;

public class DofusNetwork {
    public static SimpleNetworkWrapper network;

    public static void init() {
        int messageID = 0;
        network = NetworkRegistry.INSTANCE.newSimpleChannel("dofuxia-core");
        network.registerMessage(PacketUpdateCapabilityClient.Handler.class, PacketUpdateCapabilityClient.class, messageID++, Side.CLIENT);
        network.registerMessage(PacketRequestPlayerCapability.Handler.class, PacketRequestPlayerCapability.class, messageID++, Side.SERVER);
        System.out.println(messageID + " packets registered");
    }
}

