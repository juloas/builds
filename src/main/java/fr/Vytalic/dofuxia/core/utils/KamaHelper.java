package fr.Vytalic.dofuxia.core.utils;

import fr.Vytalic.dofuxia.core.DofuxiaCore;
import fr.Vytalic.dofuxia.core.capabilities.IDofusCapabilities;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;

public class KamaHelper {
	
	public static void addKama(final EntityPlayerMP player, int kama) {
        ((IDofusCapabilities)player.getCapability((Capability)DofuxiaCore.DOFUS_CAP, (EnumFacing)null)).addKamas(kama);
        
    }
	
	public static void removeKama (final EntityPlayerMP player, int kama) {
        ((IDofusCapabilities)player.getCapability((Capability)DofuxiaCore.DOFUS_CAP, (EnumFacing)null)).removeKamas(kama);
        
    }

}

