package fr.Vytalic.dofuxia.core.blocks;

import fr.Vytalic.dofuxia.core.utils.References;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;

public class DCBlocks {
	
	public static void init() {
		
		
	}
	
	public static Block registerBlock(Block block, FMLPreInitializationEvent e) {
		
		References.LOGGER.info("Registering Block " + block.getUnlocalizedName().substring(5));
		if(e.getSide() == Side.CLIENT) registerRender(block);
		return GameRegistry.register(block);
		
	}
	
	public static void registerRender(Block block) {
		
		ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(block), 0, new ModelResourceLocation(
																						new ResourceLocation(
																								References.MODID, 
																								block.getUnlocalizedName().substring(5)
																							), "inventory"));
		
		References.LOGGER.info("Registered render for : " + block.getUnlocalizedName().substring(5));
	
		
	}
	
}
