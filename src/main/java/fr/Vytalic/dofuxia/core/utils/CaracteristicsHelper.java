package fr.Vytalic.dofuxia.core.utils;

public class CaracteristicsHelper {
	
	public int amountToPoints(int amount) {
		
		float score = 0;
		
		for(int i = 0; i < amount; i++) {
			
			if(score > 0) {
				
				score += 1;
				
			} else if(score > 100) {
				
				score += 0.5f;
				
			} else if(score > 200) {
				
				score += 1.0f/3.0f;
				
			} else if(score > 300) {
				
				score += 1.0f/4.0f;
				
			} 
			
		}
		
		return (int) score; 
		
	}

}
