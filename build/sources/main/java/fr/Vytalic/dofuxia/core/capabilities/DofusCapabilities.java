package fr.Vytalic.dofuxia.core.capabilities;

import fr.Vytalic.dofuxia.core.DofuxiaCore;
import fr.Vytalic.dofuxia.core.network.DofusNetwork;
import fr.Vytalic.dofuxia.core.network.packet.PacketUpdateCapabilityClient;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;

public class DofusCapabilities implements ICapabilityProvider, IDofusCapabilities, INBTSerializable<NBTTagCompound> {
   
	public int level = 1;
    public long xp = 0L;
    public int kamas = 0;
    private EntityPlayer player;
    private EntityPlayer prevPlayer;

    public DofusCapabilities(EntityPlayer player) {
    	
        this.player = player;
        
    }

    public NBTTagCompound serializeNBT() {
    	
        NBTTagCompound compound = new NBTTagCompound();
        
        compound.setInteger("level", this.getLevel());
        compound.setLong("xp", this.getXp());
        compound.setInteger("kamas", this.getKamas());
        
        return compound;
        
    }

    public void deserializeNBT(NBTTagCompound nbt) {
    	
        this.setLevel(nbt.getInteger("level"));
        this.setXp(nbt.getLong("xp"));
        this.setKamas(nbt.getInteger("kamas"));
        
    }

    @Override
    public void updateClientCap(EntityPlayer player) {
    	
        DofusNetwork.network.sendToAll((IMessage)new PacketUpdateCapabilityClient(this, player));
        
    }

    public boolean hasCapability(Capability<?> capability, EnumFacing facing) {
    	
        return DofuxiaCore.DOFUS_CAP != null && capability == DofuxiaCore.DOFUS_CAP;
        
    }


	@SuppressWarnings("unchecked")
	public <T> T getCapability(Capability<T> capability, EnumFacing facing) {
  
        return (T)(DofuxiaCore.DOFUS_CAP != null && capability == DofuxiaCore.DOFUS_CAP ? this : null);
        
    }

    @Override
    public int getLevel() { return this.level; }

    @Override
    public void addLevel(int level) { this.level += level; }

    @Override
    public long getXp() { return this.xp; }

    @Override
    public void addXp(long xp) { this.xp += xp; }

    @Override
    public void setXp(long xp) { this.xp = xp; }

    @Override
    public void setLevel(int level) { this.level = level; }

    @Override
    public int getKamas() { return this.kamas; }

    @Override
    public void setKamas(int ata) { this.kamas = ata; }

    @Override
    public void removeKamas(int ata) { this.kamas -= ata; }

    @Override
    public void addKamas(int ata) { this.kamas += ata; }
    
}

