package fr.Vytalic.dofuxia.core.guis;

import fr.Vytalic.dofuxia.core.DofuxiaCore;
import fr.Vytalic.dofuxia.core.capabilities.IDofusCapabilities;
import fr.Vytalic.dofuxia.core.utils.XpHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;

@Mod.EventBusSubscriber(modid="dofuxia-core", value={Side.CLIENT})
public class GuiHealth extends Gui {
	
    private static final ResourceLocation bar = new ResourceLocation("dofuxia-core", "textures/gui/hpbar.png");

    @SubscribeEvent
    public static void renderOverlay(RenderGameOverlayEvent event) {
        
    	if (event.getType() == RenderGameOverlayEvent.ElementType.TEXT) {
            
        	Minecraft mc = Minecraft.getMinecraft();
            mc.renderEngine.bindTexture(bar);
            float oneUnit = 100.0f / mc.player.getMaxHealth();
            int currentWidth = (int)(oneUnit * mc.player.getHealth());
            mc.ingameGUI.drawTexturedModalRect(4, 5, 0, 0, 102, 8);
            mc.ingameGUI.drawTexturedModalRect(5, 5, 1, 8, currentWidth, 8);
            mc.ingameGUI.drawCenteredString(mc.fontRendererObj, "" + (int)mc.player.world.getPlayerEntityByName(mc.player.getName()).getHealth() + "/" + mc.player.world.getPlayerEntityByName(mc.player.getName()).getMaxHealth(), 104 - mc.fontRendererObj.getStringWidth("" + mc.player.getHealth() + "/" + mc.player.getMaxHealth()), 5, Integer.parseInt("EEFFEE", 16));
            
            IDofusCapabilities cap = (IDofusCapabilities)mc.player.getCapability(DofuxiaCore.DOFUS_CAP, null);
            
            mc.ingameGUI.drawString(mc.fontRendererObj, "Niveau : " + cap.getLevel(), 5, 15, Integer.parseInt("EEFFEE", 16));
            mc.ingameGUI.drawString(mc.fontRendererObj, "Xp : " + cap.getXp() + "/" + XpHelper.up[cap.getLevel()], 5, 25, Integer.parseInt("EEFFEE", 16));
            mc.ingameGUI.drawString(mc.fontRendererObj, "Kamas : " + cap.getKamas(), 5, 35, Integer.parseInt("EEFFEE", 16));
            
            mc.ingameGUI.drawString(mc.fontRendererObj, "Vitalit� : " + cap.getVitality(), 5, 60, Integer.parseInt("EEFFEE", 16));
            mc.ingameGUI.drawString(mc.fontRendererObj, "Sagesse : " + cap.getWisdom(), 5, 70, Integer.parseInt("EEFFEE", 16));
            mc.ingameGUI.drawString(mc.fontRendererObj, "Force : " + cap.getStrength(), 5, 80, Integer.parseInt("EEFFEE", 16));
            mc.ingameGUI.drawString(mc.fontRendererObj, "Intelligence : " + cap.getIntelligence(), 5, 90, Integer.parseInt("EEFFEE", 16));
            mc.ingameGUI.drawString(mc.fontRendererObj, "Chance : " + cap.getLuck(), 5, 100, Integer.parseInt("EEFFEE", 16));
            mc.ingameGUI.drawString(mc.fontRendererObj, "Agilit� : " + cap.getAgility(), 5, 110, Integer.parseInt("EEFFEE", 16));
        
        }
        
    }
    
}

