package fr.Vytalic.dofuxia.core.items;

import fr.Vytalic.dofuxia.core.DofuxiaCore;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemBase extends Item implements ItemModelProvider {
	
protected String name;
	
	public ItemBase(String name) {
		
		this.name = name;
		setUnlocalizedName(name);
		setRegistryName(name);
		
	}

	public void registerItemModel(Item item) {
		
		DofuxiaCore.proxy.registerItemRenderer(this, 0, this.name);
		
	}

	public ItemBase setCreativeTab(CreativeTabs tab) {
		
		super.setCreativeTab(tab);
		return this;
		
	}

}
