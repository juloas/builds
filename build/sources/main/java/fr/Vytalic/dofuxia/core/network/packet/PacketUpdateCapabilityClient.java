package fr.Vytalic.dofuxia.core.network.packet;

import fr.Vytalic.dofuxia.core.DofuxiaCore;
import fr.Vytalic.dofuxia.core.capabilities.IDofusCapabilities;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class PacketUpdateCapabilityClient implements IMessage {
   
	private NBTTagCompound tag;
    private String playerName;
    private double maxHealth;

    public PacketUpdateCapabilityClient() {}

    public PacketUpdateCapabilityClient(EntityPlayer player) {
    	
        this((IDofusCapabilities)player.getCapability(DofuxiaCore.DOFUS_CAP, null), player);
   
    }

    public PacketUpdateCapabilityClient(IDofusCapabilities cap, EntityPlayer player) {
    	
        this.tag = (NBTTagCompound)DofuxiaCore.DOFUS_CAP.getStorage().writeNBT(DofuxiaCore.DOFUS_CAP, cap, null);
        this.playerName = player.getGameProfile().getName();
        this.maxHealth = player.getMaxHealth();
        
    }

    public void fromBytes(ByteBuf buf) {
    	
        this.tag = ByteBufUtils.readTag((ByteBuf)buf);
        this.playerName = ByteBufUtils.readUTF8String((ByteBuf)buf);
        this.maxHealth = buf.readDouble();
        
    }

    public void toBytes(ByteBuf buf) {
    	
        ByteBufUtils.writeTag((ByteBuf)buf, (NBTTagCompound)this.tag);
        ByteBufUtils.writeUTF8String((ByteBuf)buf, (String)this.playerName);
        buf.writeDouble(this.maxHealth);
        
    }

    public static class Handler
    implements IMessageHandler<PacketUpdateCapabilityClient, IMessage> {
    	
        @SideOnly(value=Side.CLIENT)
        public IMessage onMessage(PacketUpdateCapabilityClient message, MessageContext ctx) {
        	
            Minecraft mc = FMLClientHandler.instance().getClient();
            
            mc.addScheduledTask(() -> {
            	
                for (EntityPlayer player : mc.world.playerEntities) {
                	
                    if (!player.getGameProfile().getName().equals(message.playerName) || message.tag == null) continue;
                    
                    IDofusCapabilities playerCap = (IDofusCapabilities)player.getCapability(DofuxiaCore.DOFUS_CAP, null);
                    DofuxiaCore.DOFUS_CAP.getStorage().readNBT(DofuxiaCore.DOFUS_CAP, (IDofusCapabilities)playerCap, null, (NBTBase)message.tag);
                    player.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue((double)(65 + playerCap.getLevel() * 5));
                    
                    break;
                    
                }
                
            });
            
            return null;
            
        }
        
    }

}

