package fr.Vytalic.dofuxia.core.items;

import fr.Vytalic.dofuxia.core.utils.References;
import net.minecraft.item.Item;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class DCItems {

	
	public static void init() {

	
	}

	@SuppressWarnings("unused")
	protected
	static <T extends Item> Item register(T item) {
		
		GameRegistry.register(item);
		if ((item instanceof ItemModelProvider)) {
			
			((ItemModelProvider)item).registerItemModel(item);
			
		}
		
		References.LOGGER.info("Registered Item : " + item.getUnlocalizedName().substring(5));
		
		return item;
		
	}

}
