package fr.Vytalic.dofuxia.core.proxys;

import fr.Vytalic.dofuxia.core.capabilities.DofusCapabilitiesEvent;
import net.minecraft.item.Item;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.event.FMLServerStoppingEvent;

public class CommonProxy {

	public void preInit(FMLPreInitializationEvent event) {

		// Examples
		// MinecraftForge.EVENT_BUS.register(new TechnicalGameEvent());
		// CapabilityManager.INSTANCE.register(IMana.class, new ManaStorage(), Mana.class);
		MinecraftForge.EVENT_BUS.register(new DofusCapabilitiesEvent());

	}
	
	public void init(FMLInitializationEvent event) {}
	public void postInit(FMLPostInitializationEvent event) {}
	public void serverStarting(FMLServerStartingEvent event) {}
	public void serverStopping(FMLServerStoppingEvent event) {}
	public void registerItemRenderer(Item item, int meta, String id) {}

}
