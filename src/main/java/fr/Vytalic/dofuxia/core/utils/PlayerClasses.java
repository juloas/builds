package fr.Vytalic.dofuxia.core.utils;

public enum PlayerClasses {

	ECAFLIP,
	ENIRIPSA,
	IOP,
	CRA,
	FECA,
	SACRIEUR,
	SADIDA,
	OSAMODAS,
	ENUTROF,
	SRAM,
	XELOR,
	PANDAWA,
	ROUBLARD,
	ZOBAL,
	STEAMER,
	ELIOTROPE,
	HUPPERMAGE,
	OUGINAK,
	PLAYER;

	public static int getInt(PlayerClasses player_class) {

		switch(player_class) {

		case CRA:
			return 1;
		case ECAFLIP:
			return 2;
		case ELIOTROPE:
			return 3;
		case ENIRIPSA:
			return 4;
		case ENUTROF:
			return 5;
		case FECA:
			return 6;
		case HUPPERMAGE:
			return 7;
		case IOP:
			return 8;
		case OSAMODAS:
			return 9;
		case OUGINAK:
			return 10;
		case PANDAWA:
			return 11;
		case PLAYER:
			return -1;
		case ROUBLARD:
			return 12;
		case SACRIEUR:
			return 13;
		case SADIDA:
			return 14;
		case SRAM:
			return 15;
		case STEAMER:
			return 16;
		case XELOR:
			return 17;
		case ZOBAL:
			return 18;
		default:
			return 0;

		}

	}

	public static PlayerClasses getPlayerClass(int int_class) {
		
		switch(int_class) {
		
		case -1:
			return PlayerClasses.PLAYER;
		case 0:
			return null;
		case 1:
			return PlayerClasses.CRA;
		case 2:
			return PlayerClasses.ECAFLIP;
		case 3:
			return PlayerClasses.ELIOTROPE;
		case 4:
			return PlayerClasses.ENIRIPSA;
		case 5:
			return PlayerClasses.ENUTROF;
		case 6:
			return PlayerClasses.FECA;
		case 7:
			return PlayerClasses.HUPPERMAGE;
		case 8:
			return PlayerClasses.IOP;
		case 9:
			return PlayerClasses.OSAMODAS;
		case 10:
			return PlayerClasses.OUGINAK;
		case 11:
			return PlayerClasses.PANDAWA;
		case 12:
			return PlayerClasses.ROUBLARD;
		case 13:
			return PlayerClasses.SACRIEUR;
		case 14:
			return PlayerClasses.SADIDA;
		case 15:
			return PlayerClasses.SRAM;
		case 16:
			return PlayerClasses.STEAMER;
		case 17:
			return PlayerClasses.XELOR;
		case 18: 
			return PlayerClasses.ZOBAL;
		default:
			return null;
		
		}
		
	}

}
