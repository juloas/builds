package fr.Vytalic.dofuxia.core.proxys;

import fr.Vytalic.dofuxia.core.events.EventHandler;
import fr.Vytalic.dofuxia.core.guis.GuiHealth;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.event.FMLServerStoppingEvent;

public class ClientProxy extends CommonProxy {
	
	public static EventHandler eh = new EventHandler();
    public static GuiHealth gh = new GuiHealth();
	
	@Override
	public void preInit(FMLPreInitializationEvent event) {
		
		super.preInit(event);
			
	}

	@Override
	public void init(FMLInitializationEvent event) {

		super.init(event);

	}
	
	@Override
	public void postInit(FMLPostInitializationEvent event) {

		super.postInit(event);

	}
	
	@Override
	public void serverStarting(FMLServerStartingEvent event) {

		super.serverStarting(event);

	}
	
	@Override
	public void serverStopping(FMLServerStoppingEvent event) {

		super.serverStopping(event);

	}
	
	@Override
	public void registerItemRenderer(Item item, int meta, String id) {

		ModelLoader.setCustomModelResourceLocation(item, meta, new ModelResourceLocation("dofuxia-core:" + id, "inventory"));

	}

}
