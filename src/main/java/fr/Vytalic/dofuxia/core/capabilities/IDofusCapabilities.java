package fr.Vytalic.dofuxia.core.capabilities;

import fr.Vytalic.dofuxia.core.utils.PlayerClasses;
import fr.Vytalic.dofuxia.core.utils.enums.DofusJobs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;

public interface IDofusCapabilities {
	
    public void updateClientCap(EntityPlayer var1);

    public int getLevel();

    public void addLevel(int var1);

    public void setLevel(int var1);

    public long getXp();

    public void addXp(long var1);

    public void setXp(long var1);

    public int getKamas();

    public void setKamas(int var1);

    public void removeKamas(int var1);

    public void addKamas(int var1);
    
    public void setVitality(int vit);
    
    public int getVitality();
    
    public void setWisdom(int wisdom);
    
    public int getWisdom();
    
    public void setStrength(int strength);
    
    public int getStrength();
    
    public void setIntelligence(int intel);
    
    public int getIntelligence();
    
    public void setLuck(int luck);
    
    public int getLuck();
    
    public void setAgility(int agility);
    
    public int getAgility();
    
    public void setPlayerClass(int player_class);
    
    public PlayerClasses getPlayerClass();

	public NBTBase getTag();

	public void synch(NBTTagCompound tag);

	void setPlayerClass(PlayerClasses player_class);

	public void synch(IDofusCapabilities originalCapa);
	
	public void setXpAlchimiste(long xp);
	public void addXpAlchimiste(long xp, EntityPlayer p);
	public long getXpAlchimiste();
	
	public void setXpBijoutier(long xp);
	public void addXpBijoutier(long xp, EntityPlayer p);
	public long getXpBijoutier();
	
	public void setXpBricoleur(long xp);
	public void addXpBricoleur(long xp, EntityPlayer p);
	public long getXpBricoleur();
	
	public void setXpBucheron(long xp);
	public void addXpBucheron(long xp, EntityPlayer p);
	public long getXpBucheron();

	public void setXpChasseur(long xp);
	public void addXpChasseur(long xp, EntityPlayer p);
	public long getXpChasseur();
	
	public void setXpCordomage(long xp);
	public void addXpCordomage(long xp, EntityPlayer p);
	public long getXpCordomage();
	
	public void setXpCordonnier(long xp);
	public void addXpCordonnier(long xp, EntityPlayer p);
	public long getXpCordonnier();
	
	public void setXpCostumage(long xp);
	public void addXpCostumage(long xp, EntityPlayer p);
	public long getXpCostumage();
	
	public void setXpFacomage(long xp);
	public void addXpFacomage(long xp, EntityPlayer p);
	public long getXpFacomage();
	
	public void setXpFaconeur(long xp);
	public void addXpFaconeur(long xp, EntityPlayer p);
	public long getXpFaconeur();
	
	public void setXpForgeron(long xp);
	public void addXpForgeron(long xp, EntityPlayer p);
	public long getXpForgeron();
	
	public void setXpForgemage(long xp);
	public void addXpForgemage(long xp, EntityPlayer p);
	public long getXpForgemage();
	
	public void setXpJoaillomage(long xp);
	public void addXpJoaillomage(long xp, EntityPlayer p);
	public long getXpJoaillomage();
	
	public void setXpMineur(long xp);
	public void addXpMineur(long xp, EntityPlayer p);
	public long getXpMineur();
	
	public void setXpPaysan(long xp);
	public void addXpPaysan(long xp, EntityPlayer p);
	public long getXpPaysan();

	public void setXpPecheur(long xp);
	public void addXpPecheur(long xp, EntityPlayer p);
	public long getXpPecheur();
	
	public void setXpSculptemage(long xp);
	public void addXpSculptemage(long xp, EntityPlayer p);
	public long getXpSculptemage();
	
	public void setXpSculpteur(long xp);
	public void addXpSculpteur(long xp, EntityPlayer p);
	public long getXpSculpteur();
	
	public void setXpTailleur(long xp);
	public void addXpTailleur(long xp, EntityPlayer p);
	public long getXpTailleur();

	public void addXpJob(int xpDrop, EntityPlayer playerIn, DofusJobs rt);
	public long getXpJob(DofusJobs rt);
    
}

