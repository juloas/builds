package fr.Vytalic.dofuxia.core.guis;

import fr.Vytalic.dofuxia.core.DofuxiaCore;
import fr.Vytalic.dofuxia.core.capabilities.IDofusCapabilities;
import fr.Vytalic.dofuxia.core.utils.XpHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;

@Mod.EventBusSubscriber(modid="dofucraft", value={Side.CLIENT})
public class GuiHealth
extends Gui {
	
    private ResourceLocation h;
    private static final ResourceLocation bar = new ResourceLocation("dofucraft", "textures/gui/hpbar.png");


    @SubscribeEvent
    public static void renderOverlay(RenderGameOverlayEvent event) {
        
    	if (event.getType() == RenderGameOverlayEvent.ElementType.TEXT) {
            
        	Minecraft mc = Minecraft.getMinecraft();
            mc.renderEngine.bindTexture(bar);
            float oneUnit = 100.0f / mc.player.getMaxHealth();
            int currentWidth = (int)(oneUnit * mc.player.getHealth());
            mc.ingameGUI.drawTexturedModalRect(0, 0, 0, 0, 102, 8);
            mc.ingameGUI.drawTexturedModalRect(1, 0, 1, 8, currentWidth, 8);
            mc.ingameGUI.drawCenteredString(mc.fontRendererObj, "" + (int)mc.player.getHealth() + "/" + mc.player.getMaxHealth(), 100 - mc.fontRendererObj.getStringWidth("" + mc.player.getHealth() + "/" + mc.player.getMaxHealth()), 0, Integer.parseInt("EEFFEE", 16));
            mc.ingameGUI.drawString(mc.fontRendererObj, "Niveau : " + ((IDofusCapabilities)mc.player.getCapability(DofuxiaCore.DOFUS_CAP, null)).getLevel(), 5, 10, Integer.parseInt("EEFFEE", 16));
            mc.ingameGUI.drawString(mc.fontRendererObj, "Xp : " + ((IDofusCapabilities)mc.player.getCapability(DofuxiaCore.DOFUS_CAP, null)).getXp() + "/" + XpHelper.up[((IDofusCapabilities)mc.player.getCapability(DofuxiaCore.DOFUS_CAP, null)).getLevel()], 5, 20, Integer.parseInt("EEFFEE", 16));
            mc.ingameGUI.drawString(mc.fontRendererObj, "Kamas : " + ((IDofusCapabilities)mc.player.getCapability(DofuxiaCore.DOFUS_CAP, null)).getKamas(), 5, 30, Integer.parseInt("EEFFEE", 16));
        
        }
        
    }
    
}

