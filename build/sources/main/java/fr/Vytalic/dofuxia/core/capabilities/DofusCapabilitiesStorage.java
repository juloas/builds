package fr.Vytalic.dofuxia.core.capabilities;

import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;

public class DofusCapabilitiesStorage implements Capability.IStorage<IDofusCapabilities> {
    
	public NBTBase writeNBT(Capability<IDofusCapabilities> capability, IDofusCapabilities instance, EnumFacing side) {
        
    	NBTTagCompound tag = new NBTTagCompound();
        
        tag.setInteger("level", instance.getLevel());
        tag.setLong("xp", instance.getXp());
        tag.setInteger("kamas", instance.getKamas());
       
        return tag;
        
    }

    public void readNBT(Capability<IDofusCapabilities> capability, IDofusCapabilities instance, EnumFacing side, NBTBase nbt) {
       
    	NBTTagCompound tag = (NBTTagCompound)nbt;
        
        instance.setLevel(tag.getInteger("level"));
        instance.setXp(tag.getLong("xp"));
        instance.setKamas(tag.getInteger("kamas"));
        
    }
    
}

